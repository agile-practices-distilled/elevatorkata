using System.Collections.Generic;

namespace Elevator
{
    public class Trip : SortedSet<Floor>
    {
        public Direction Direction { get; }

        public Trip(Direction direction) : base(new FlorComparer())
        {
            Direction = direction;
        }

        public override string ToString()
        {
            var floorHistory = Direction == Direction.Up ? this : this.Reverse();

            return string.Join("-", floorHistory);
        }
    }
}