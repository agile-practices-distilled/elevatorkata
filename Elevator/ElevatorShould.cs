using Xunit;

namespace Elevator
{
    public class ElevatorShould
    {
        private readonly Elevator _elevator;

        public ElevatorShould()
        {
            _elevator = new Elevator();
            Assert.Equal(string.Empty, _elevator.FloorHistory());
        }
        
        [Fact]
        public void KeepTrackOfVisitedFloors()
        {
            _elevator.Move(Floor.Basement, Floor.Floor1);
            
            Assert.Equal("Basement-Floor1", _elevator.FloorHistory());
        }
        
        [Fact]
        public void QueueCallsGoingDown()
        {
            _elevator.Move(Floor.Floor3, Floor.Basement);
            _elevator.Move(Floor.Floor2, Floor.Basement);
            _elevator.Move(Floor.Floor1, Floor.Ground);
            
            Assert.Equal("Floor3-Floor2-Floor1-Ground-Basement", _elevator.FloorHistory());
        }
        
        [Fact]
        public void QueueCallsGoingUp()
        {
            _elevator.Move(Floor.Basement, Floor.Floor3);
            _elevator.Move(Floor.Floor2, Floor.Floor3);
            _elevator.Move(Floor.Floor1, Floor.Floor2);
            
            Assert.Equal("Basement-Floor1-Floor2-Floor3", _elevator.FloorHistory());
        }
        
        [Fact]
        public void QueueCallsOppositeDirectionUntilPreviousIsFinished()
        {
            _elevator.Move(Floor.Floor3, Floor.Basement);
            _elevator.Move(Floor.Ground, Floor.Basement);
            _elevator.Move(Floor.Floor2, Floor.Basement);
            _elevator.Move(Floor.Floor1, Floor.Floor3);
            
            Assert.Equal("Floor3-Floor2-Ground-Basement-Floor1-Floor3", _elevator.FloorHistory());
        }
    }
}