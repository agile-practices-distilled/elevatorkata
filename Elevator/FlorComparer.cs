using System.Collections.Generic;

namespace Elevator
{
    class FlorComparer : IComparer<Floor>
    {
        public int Compare(Floor x, Floor y)
        {
            return x - y;
        }
    }
}