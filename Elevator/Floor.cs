namespace Elevator
{
    public enum Floor
    {
        Basement = -1,
        Ground = 0,
        Floor1 = 1,
        Floor2 = 2,
        Floor3 = 3
    }
}