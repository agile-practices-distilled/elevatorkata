﻿using System.Collections.Generic;
using System.Linq;

namespace Elevator
{
    public class Elevator
    {
        private readonly List<Trip> _trips;

        public Elevator()
        {
            _trips = new List<Trip>();
        }

        public void Move(Floor sourceFloor, Floor targetFloor)
        {
            var currentDirection = GetCurrentDirection(sourceFloor, targetFloor);
            
            if (!_trips.Any() || IsAMoveInTheOppositeDirection(currentDirection))
            {
                _trips.Add(new Trip(currentDirection));
            }
            
            _trips.Last().Add(sourceFloor);
            _trips.Last().Add(targetFloor);
        }

        private bool IsAMoveInTheOppositeDirection(Direction currentDirection)
        {
            return _trips.Last().Direction != currentDirection;
        }

        private static Direction GetCurrentDirection(Floor sourceFloor, Floor targetFloor)
        {
            return targetFloor > sourceFloor ? Direction.Up : Direction.Down;
        }

        public string FloorHistory()
        {
            if (!_trips.Any()) return string.Empty;
            
            return string.Join("-", _trips);
        }
    }
}